const express = require('express');
const app = express();
var request = require('request');
//const isInteger = require("is-integer");
const bodyParser = require('body-parser');
const refno = require('./library/genrefno');
//const duplicate = require('./library/check_duplicate');
//const config = require('./library/config');
const endpoint = require('./library/endpoint')
const datetime = require('./library/datetime');
//const schema = require('./library/schema');
//const checkPartnerNBR = require('./library/checkPartnerNBR');
const getMCard = require('./library/checkMCard');
const schema = require('./library/checkSchema');
const transaction = require('./library/transaction');
const get_mcrr2p = require('./library/get_mcrr2p');
const insert_mcrr2p = require('./library/insert_mcrr2p');
const update_point = require('./library/update_point');

app.listen(8121, function () {
	console.log('app listening on port 8121!');
})

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));

app.post('/redeempoint', async function (req, res) {
	// datetime
	//res.status(500).json(req.body);
	var dtf = await datetime.getdatetime();


	// check valid
	if (typeof req.body.POINTBURN_TYPE == 'undefined' || typeof req.body.POINTBURN_TYPE != 'string') {
		console.log('TYPE : undefined');
		res.status(200);
		res.json({
			"RESP_SYSCDE": 200,
			"RESP_DATETIME": dtf,
			"RESP_CDE": 401,
			"RESP_MSG": "Missing Required Field"
		});
		return;
	} else if (req.body.POINTBURN_TYPE != 'CC' && req.body.POINTBURN_TYPE != 'DP' && req.body.POINTBURN_TYPE != 'MI' && req.body.POINTBURN_TYPE != 'PR' && req.body.POINTBURN_TYPE != 'SP') {
		console.log('TYPE : unknown');
		res.status(200);
		res.json({
			"RESP_SYSCDE": 200,
			"RESP_DATETIME": dtf,
			"RESP_CDE": 402,
			"RESP_MSG": "Invalid Format"
		});
		return;
	}

	// check schema
	schema.checkSchema(req,res,"redeem_"+req.body.POINTBURN_TYPE,dtf);

	// flow
	//let checkPartner = await checkPartnerNBR.checkPartnerNBR(req, res, dtf);
	//if (checkPartner == true) {
		//res.json({"aa":"asa"});
		let MCard = await getMCard.checkMCard(req, res, dtf);
		//MCard = JSON.parse(MCard);
		var cal_POINTBURN = 0;
		if (req.body.POINTBURN_TYPE == "DP") {
			cal_POINTBURN = parseInt(req.body.POINTBURN_MPOINT);
		} else if (req.body.POINTBURN_TYPE == "MI") {
			cal_POINTBURN = parseInt(req.body.POINTBURN_MPOINT) * parseInt(req.body.POINTBURN_MILE);
		} else if (req.body.POINTBURN_TYPE == "CC") {
			cal_POINTBURN = parseInt(req.body.POINTBURN_MPOINT) * parseInt(req.body.POINTBURN_PIECE);
		} else if (req.body.POINTBURN_TYPE == "SP") {
			cal_POINTBURN = parseInt(req.body.POINTBURN_MPOINT) * parseInt(req.body.POINTBURN_PIECE);
		} else if (req.body.POINTBURN_TYPE == "PR") {
			cal_POINTBURN = parseInt(req.body.POINTBURN_MPOINT) * parseInt(req.body.POINTBURN_PIECE);

		} else {
			console.log('Unknown type');
			res.json({
				"RESP_SYSCDE": 200,
				"RESP_DATETIME": dtf,
				"RESP_CDE": 402,
				"RESP_MSG": "Invalid Format"
			});
			return;
		}

		if (parseInt(MCard[0].MBPOINT) < cal_POINTBURN) {
			res.json({
				"RESP_SYSCDE": 200,
				"RESP_DATETIME": dtf,
				"RESP_CDE": 201,
				"RESP_MSG": "Insufficient MPoint"
			});
			return;
		}
		console.log(req.body.POINTBURN_TYPE);
		console.log(MCard[0].MBPOINR);
		var cal_MPOINR = parseInt(MCard[0].MBPOINR) + cal_POINTBURN;
		console.log(cal_MPOINR);
		console.log(MCard[0].MBPOINC);
		var cal_MBPOINT = parseInt(MCard[0].MBPOINC) - cal_MPOINR;
		console.log(cal_MPOINR);
		console.log(cal_MBPOINT);
		console.log(MCard[0].MBCODE);

		// log
		var today = new Date();
		var date_str = await today.getFullYear().toString() + ((today.getMonth() + 1) < 10 ? '0' : '').toString() + (today.getMonth() + 1).toString() + (today.getDate() < 10 ? '0' : '').toString() + today.getDate();
		var date_str4 = date_str.substr(2, 4);
		var mtyr = await req.body.POINTBURN_TYPE;
		var mrec = mtyr + date_str4;
		console.log(mrec);
		var mrec_n = '';
		//var mrec_c = '';
		try {

			var rcp_result = await refno.genrefno("99", mtyr, res);

			mrec_n = await rcp_result.Return_ReceiptNo;
			//res.json({"hello":endpoint.mds_mcard_redeempoint.protocol});

			request({
				url: '' + endpoint.mds_mcard_redeempoint.protocol + '://' + endpoint.mds_mcard_redeempoint.url + ':' + endpoint.mds_mcard_redeempoint.port + '/add_log/' + mrec_n + '/' + dtf + '/' + MCard[0].MBAPP +  '/' + MCard[0].MBCODE,
				method: "POST",
				json: true,
				body: req.body
			}, async function (error, response, body) {
				console.log(response);
				//res.send(error);
				//res.send(response);
				if (response.statusCode == 200) {
					let updatepoint = await update_point.update_point(req, res, cal_MPOINR, cal_MBPOINT, MCard[0].MBCODE, dtf);
					if (updatepoint == true) {
						res.json({
							"RESP_SYSCDE": 200,
							"RESP_DATETIME": dtf,
							"RESP_CDE": 101,
							"RESP_MSG": "Success",
							"MCARD_NUM": MCard[0].MBCODE,
							"CARD_TYPE": MCard[0].MBMEMC,
							"CARD_EXPIRY_DATE": MCard[0].MBEXP,
							"CARD_POINT_BALANCE": cal_MBPOINT.toString(),
							"CARD_POINT_EXPIRY": MCard[0].MBCEXP,
							"CARD_POINT_EXP_DATE": MCard[0].MBDATT,
							"POINTBURN_MPOINT_SUCCESS": cal_POINTBURN
						});
					}
				} else if (response.statusCode == 500) {
					res.json(body);
				} else {
					res.json({
						"RESP_SYSCDE": 200,
						"RESP_DATETIME": dtf,
						"RESP_CDE": 500,
						"RESP_MSG": "log error"
					});
				}


			});


		} catch (error) {
			res.status(200);
			res.json({
				"RESP_SYSCDE": 200,
				"RESP_DATETIME": dtf,
				"RESP_CDE": 500,
				"RESP_MSG": "Log Failed : MCRTA3P",
				"MCARD_NUM": "",
				"CARD_TYPE": 0,
				"CARD_EXPIRY_DATE": "",
				"CARD_POINT_BALANCE": "",
				"CARD_POINT_EXPIRY": "",
				"CARD_POINT_EXP_DATE": "",
				"POINTBURN_MPOINT_SUCCESS": 0
			});
		}

	/*} else {
		console.log('No partner');
		res.json({
			"RESP_SYSCDE": 200,
			"RESP_DATETIME": dtf,
			"RESP_CDE": 301,
			"RESP_MSG": "Not success/ Not found Partner ID/Partner NBR",
			"MCARD_NUM": "",
			"CARD_TYPE": 0,
			"CARD_EXPIRY_DATE": "",
			"CARD_POINT_BALANCE": "",
			"CARD_POINT_EXPIRY": "",
			"CARD_POINT_EXP_DATE": "",
			"DEMO_TH_TITLE": "",
			"DEMO_TH_NAME": "",
			"DEMO_TH_SURNAME": "",
			"DEMO_EN_TITLE": "",
			"DEMO_EN_NAME": "",
			"DEMO_EN_SURNAME": ""
		});
		return;
	}*/

})

app.post('/add_log/:ReceiptNo/:dtf/:MBAPP/:MBCODE', async function (req, res) {
	//var date = dtf.substr(0, 8);
	//(req,res,dtf,MBAPP,MBAGEN,ReceiptNo,MBCODE)
	//res.status(500).json({"dtf":req.params.dtf});
	try {
		let trans_result = await transaction.transaction(req, res, req.params.dtf, req.params.MBAPP, req.params.ReceiptNo, req.params.MBCODE);
		if (trans_result == true) {
			//res.status(500);
			//res.json({"hello":"tran"});
			let p2_result = await get_mcrr2p.get_mcrr2p(req, res, req.params.dtf, req.params.MBCODE);
			//res.status(500);
			//res.json(p2_result);
			if (p2_result.length < 1) {
				try {
					let p2_insert_result = await insert_mcrr2p.insert_mcrr2p(req, res, req.params.dtf, req.params.MBCODE);
					//update_point.update_point(req,res);
					res.status(200).json({
						"RESP_SYSCDE": 200,
						"RESP_DATETIME": req.params.dtf,
						"RESP_CDE": 200
					});

				} catch (err) {
					console.log(err);
					console.log('Log Failed(INSERT) : MCRR2P');
					res.status(500);
					res.json({
						"RESP_SYSCDE": 200,
						"RESP_DATETIME": req.params.dtf,
						"RESP_CDE": 500,
						"RESP_MSG": "Log Failed : MCRTA3P",
						"MCARD_NUM": "",
						"CARD_TYPE": 0,
						"CARD_EXPIRY_DATE": "",
						"CARD_POINT_BALANCE": "",
						"CARD_POINT_EXPIRY": "",
						"CARD_POINT_EXP_DATE": "",
						"POINTBURN_MPOINT_SUCCESS": 0
					});
					res.end()
					return;
				}
			} else {
				res.status(200).json({
					"RESP_SYSCDE": 200,
					"RESP_DATETIME": req.params.dtf,
					"RESP_CDE": 200
				});
			}

		} else {
			res.status(500);
			res.json({
				"RESP_SYSCDE": 200,
				"RESP_DATETIME": req.params.dtf,
				"RESP_CDE": 500,
				"RESP_MSG": "Log Failed : MCRR1P",
				"MCARD_NUM": "",
				"CARD_TYPE": 0,
				"CARD_EXPIRY_DATE": "",
				"CARD_POINT_BALANCE": "",
				"CARD_POINT_EXPIRY": "",
				"CARD_POINT_EXP_DATE": "",
				"POINTBURN_MPOINT_SUCCESS": 0
			});
		}
	} catch (err) {
		res.status(500);
		res.json({
			"RESP_SYSCDE": 200,
			"RESP_DATETIME": req.params.dtf,
			"RESP_CDE": 500,
			"RESP_MSG": "Log Failed : MCRR1P",
			"MCARD_NUM": "",
			"CARD_TYPE": 0,
			"CARD_EXPIRY_DATE": "",
			"CARD_POINT_BALANCE": "",
			"CARD_POINT_EXPIRY": "",
			"CARD_POINT_EXP_DATE": "",
			"POINTBURN_MPOINT_SUCCESS": 0
		});
	}
})