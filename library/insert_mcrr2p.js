const express = require('express');
const router = express.Router();
const config = require('./config');
var rp = require('request-promise');

const pool = require('node-jt400').pool(config);
//  POST /api/mcard/:MBCODE
module.exports.insert_mcrr2p = (async function (req, res, dtf, MBCODE) {
        //router.get('/:DATE/:MBCODE', function(req,res){
        var date = dtf.substr(0, 8);
        var point_log_stmt_2p = "insert into MBRFLIB/MCRR2P";
        point_log_stmt_2p += "(MBDAT,MBCODE,MBFLG)";
        point_log_stmt_2p += " values(?,?,?)";
        var point_log_params_2p = [
                parseInt(date),
                MBCODE,
                ''
        ];

        var result = await pool.insertAndGetId(point_log_stmt_2p, point_log_params_2p);
        //console.log(result.length);
        console.log(result);
        res.status(500).json(result);
        if (result == 0) {
                return true;
        } else {
                console.log('Log Failed(INSERT) : MCRR2P');
                res.status(500);
                res.json({
                        "RESP_SYSCDE": 200,
                        "RESP_DATETIME": dtf,
                        "RESP_CDE": 500,
                        "RESP_MSG": "Log Failed(INSERT) : MCRR2P",
                        "MCARD_NUM": "",
                        "CARD_TYPE": 0,
                        "CARD_EXPIRY_DATE": "",
                        "CARD_POINT_BALANCE": "",
                        "CARD_POINT_EXPIRY": "",
                        "CARD_POINT_EXP_DATE": "",
                        "POINTBURN_MPOINT_SUCCESS": 0
                });
        }
});