const express = require('express');
const router = express.Router();
const config = require('./config');
const Joi = require('joi');
//console.log(config.app.hah);
//console.log(config.db.user);
const _mandatory_template = {
    "redeem_DP": Joi.object().keys({
        MBCODE: Joi.any().required(),
        POINTBURN_TYPE: Joi.any().required(),
        POINTBURN_FLAG: Joi.any().required(),
        POINTBURN_BRANCH: Joi.any().required(),
        POINTBURN_DEPT: Joi.any().required(),
        POINTBURN_PROMO_NAME: Joi.any().required(),
        POINTBURN_ITEM_CODE: Joi.any().required(),
        POINTBURN_ITEM_NAME: Joi.any().optional(),
        POINTBURN_PROMO_NUM: Joi.any().required(),
        POINTBURN_EDC_SHOP_NAME: Joi.any().required(),
        POINTBURN_VENDER: Joi.any().optional(),
        POINTBURN_REFERENCE_NUM: Joi.any().required(),
        POINTBURN_APPV_NUM: Joi.any().required(),
        POINTBURN_EDC_RATE: Joi.any().required(),
        POINTBURN_EDC_SALE_AMOUNT: Joi.any().required(),
        POINTBURN_EDC_DISCOUNT_AMT: Joi.any().required(),
        POINTBURN_EDC_TERMINAL: Joi.any().required(),
        POINTBURN_MPOINT: Joi.any().required(),
        POINTBURN_PIECE: Joi.any().optional(),
        POINTBURN_ITEM_AMT: Joi.any().optional(),
        POINTBURN_ITEM_ADD_AMT: Joi.any().optional(),
        POINTBURN_MILE: Joi.any().optional(),
        POINTBURN_AIRLINECODE: Joi.any().optional(),
        REF_WEB_NUM: Joi.any().required(),
        SECURE_CODE: Joi.any().required()
    }),
    "redeem_MI": Joi.object().keys({
        MBCODE: Joi.any().required(),
        POINTBURN_TYPE: Joi.any().required(),
        POINTBURN_FLAG: Joi.any().required(),
        POINTBURN_BRANCH: Joi.any().required(),
        POINTBURN_DEPT: Joi.any().optional(),
        POINTBURN_PROMO_NAME: Joi.any().optional(),
        POINTBURN_ITEM_CODE: Joi.any().required(),
        POINTBURN_ITEM_NAME: Joi.any().required(),
        POINTBURN_PROMO_NUM: Joi.any().optional(),
        POINTBURN_EDC_SHOP_NAME: Joi.any().optional(),
        POINTBURN_VENDER: Joi.any().optional(),
        POINTBURN_REFERENCE_NUM: Joi.any().required(),
        POINTBURN_APPV_NUM: Joi.any().optional(),
        POINTBURN_EDC_RATE: Joi.any().optional(),
        POINTBURN_EDC_SALE_AMOUNT: Joi.any().optional(),
        POINTBURN_EDC_DISCOUNT_AMT: Joi.any().optional(),
        POINTBURN_EDC_TERMINAL: Joi.any().optional(),
        POINTBURN_MPOINT: Joi.any().required(),
        POINTBURN_PIECE: Joi.any().optional(),
        POINTBURN_ITEM_AMT: Joi.any().optional(),
        POINTBURN_ITEM_ADD_AMT: Joi.any().optional(),
        POINTBURN_MILE: Joi.any().required(),
        POINTBURN_AIRLINECODE: Joi.any().required(),
        REF_WEB_NUM: Joi.any().required(),
        SECURE_CODE: Joi.any().required()
    }),
    "redeem_CC": Joi.object().keys({
        MBCODE: Joi.any().required(),
        POINTBURN_TYPE: Joi.any().required(),
        POINTBURN_FLAG: Joi.any().required(),
        POINTBURN_BRANCH: Joi.any().required(),
        POINTBURN_DEPT: Joi.any().optional(),
        POINTBURN_PROMO_NAME: Joi.any().optional(),
        POINTBURN_ITEM_CODE: Joi.any().required(),
        POINTBURN_ITEM_NAME: Joi.any().required(),
        POINTBURN_PROMO_NUM: Joi.any().optional(),
        POINTBURN_EDC_SHOP_NAME: Joi.any().optional(),
        POINTBURN_VENDER: Joi.any().optional(),
        POINTBURN_REFERENCE_NUM: Joi.any().required(),
        POINTBURN_APPV_NUM: Joi.any().optional(),
        POINTBURN_EDC_RATE: Joi.any().optional(),
        POINTBURN_EDC_SALE_AMOUNT: Joi.any().optional(),
        POINTBURN_EDC_DISCOUNT_AMT: Joi.any().optional(),
        POINTBURN_EDC_TERMINAL: Joi.any().optional(),
        POINTBURN_MPOINT: Joi.any().required(),
        POINTBURN_PIECE: Joi.any().required(),
        POINTBURN_ITEM_AMT: Joi.any().required(),
        POINTBURN_ITEM_ADD_AMT: Joi.any().optional(),
        POINTBURN_MILE: Joi.any().optional(),
        POINTBURN_AIRLINECODE: Joi.any().optional(),
        REF_WEB_NUM: Joi.any().required(),
        SECURE_CODE: Joi.any().required()
    }),
    "redeem_SP": Joi.object().keys({
        MBCODE: Joi.any().required(),
        POINTBURN_TYPE: Joi.any().required(),
        POINTBURN_FLAG: Joi.any().required(),
        POINTBURN_BRANCH: Joi.any().required(),
        POINTBURN_DEPT: Joi.any().optional(),
        POINTBURN_PROMO_NAME: Joi.any().optional(),
        POINTBURN_ITEM_CODE: Joi.any().required(),
        POINTBURN_ITEM_NAME: Joi.any().required(),
        POINTBURN_PROMO_NUM: Joi.any().optional(),
        POINTBURN_EDC_SHOP_NAME: Joi.any().optional(),
        POINTBURN_VENDER: Joi.any().required(),
        POINTBURN_REFERENCE_NUM: Joi.any().required(),
        POINTBURN_APPV_NUM: Joi.any().optional(),
        POINTBURN_EDC_RATE: Joi.any().optional(),
        POINTBURN_EDC_SALE_AMOUNT: Joi.any().optional(),
        POINTBURN_EDC_DISCOUNT_AMT: Joi.any().optional(),
        POINTBURN_EDC_TERMINAL: Joi.any().optional(),
        POINTBURN_MPOINT: Joi.any().required(),
        POINTBURN_PIECE: Joi.any().required(),
        POINTBURN_ITEM_AMT: Joi.any().optional(),
        POINTBURN_ITEM_ADD_AMT: Joi.any().required(),
        POINTBURN_MILE: Joi.any().optional(),
        POINTBURN_AIRLINECODE: Joi.any().optional(),
        REF_WEB_NUM: Joi.any().required(),
        SECURE_CODE: Joi.any().required()
    }),
    "redeem_PR": Joi.object().keys({
        MBCODE: Joi.any().required(),
        POINTBURN_TYPE: Joi.any().required(),
        POINTBURN_FLAG: Joi.any().required(),
        POINTBURN_BRANCH: Joi.any().required(),
        POINTBURN_DEPT: Joi.any().optional(),
        POINTBURN_PROMO_NAME: Joi.any().optional(),
        POINTBURN_ITEM_CODE: Joi.any().required(),
        POINTBURN_ITEM_NAME: Joi.any().required(),
        POINTBURN_PROMO_NUM: Joi.any().optional(),
        POINTBURN_EDC_SHOP_NAME: Joi.any().optional(),
        POINTBURN_VENDER: Joi.any().required(),
        POINTBURN_REFERENCE_NUM: Joi.any().required(),
        POINTBURN_APPV_NUM: Joi.any().optional(),
        POINTBURN_EDC_RATE: Joi.any().optional(),
        POINTBURN_EDC_SALE_AMOUNT: Joi.any().optional(),
        POINTBURN_EDC_DISCOUNT_AMT: Joi.any().optional(),
        POINTBURN_EDC_TERMINAL: Joi.any().optional(),
        POINTBURN_MPOINT: Joi.any().required(),
        POINTBURN_PIECE: Joi.any().required(),
        POINTBURN_ITEM_AMT: Joi.any().optional(),
        POINTBURN_ITEM_ADD_AMT: Joi.any().optional(),
        POINTBURN_MILE: Joi.any().optional(),
        POINTBURN_AIRLINECODE: Joi.any().optional(),
        REF_WEB_NUM: Joi.any().required(),
        SECURE_CODE: Joi.any().required()
    })
};

const _template = {
    "redeem": Joi.object().keys({
        POINTBURN_TYPE: Joi.any().required(),
    }),
    "redeem_DP": Joi.object().keys({
        MBCODE: Joi.string().allow('').max(50),
        POINTBURN_TYPE: Joi.string().allow('').max(2),
        POINTBURN_FLAG: Joi.string().allow('').max(1),
        POINTBURN_BRANCH: Joi.number().allow('').max(99),
        POINTBURN_DEPT: Joi.string().allow('').max(5),
        POINTBURN_PROMO_NAME: Joi.string().allow('').max(20),
        POINTBURN_ITEM_CODE: Joi.string().allow('').max(8),
        POINTBURN_ITEM_NAME: Joi.string().allow('').max(45),
        POINTBURN_PROMO_NUM: Joi.string().allow('').max(4),
        POINTBURN_EDC_SHOP_NAME: Joi.string().allow('').max(50),
        POINTBURN_VENDER: Joi.string().allow('').max(5),
        POINTBURN_REFERENCE_NUM: Joi.string().allow('').max(20),
        POINTBURN_APPV_NUM: Joi.string().allow('').max(6),
        POINTBURN_EDC_RATE: Joi.string().allow('').max(12),
        POINTBURN_EDC_SALE_AMOUNT: Joi.number().allow('').max(99999999999999),
        POINTBURN_EDC_DISCOUNT_AMT: Joi.number().allow('').max(99999999999999),
        POINTBURN_EDC_TERMINAL: Joi.string().allow('').max(8),
        POINTBURN_MPOINT: Joi.number().allow('').max(999999999999),
        POINTBURN_PIECE: Joi.number().allow('').max(9999),
        POINTBURN_ITEM_AMT: Joi.number().allow('').max(99999999999999),
        POINTBURN_ITEM_ADD_AMT: Joi.number().allow('').max(99999999999999),
        POINTBURN_MILE: Joi.number().allow('').max(999999999999),
        POINTBURN_AIRLINECODE: Joi.string().allow('').max(10),
        REF_WEB_NUM: Joi.string().allow('').max(21),
        SECURE_CODE: Joi.string().allow('').max(21)
    }),
    "redeem_MI": Joi.object().keys({
        MBCODE: Joi.string().allow('').max(50),
        POINTBURN_TYPE: Joi.string().allow('').max(2),
        POINTBURN_FLAG: Joi.string().allow('').max(1),
        POINTBURN_BRANCH: Joi.number().allow('').max(99),
        POINTBURN_DEPT: Joi.string().allow('').max(5),
        POINTBURN_PROMO_NAME: Joi.string().allow('').max(20),
        POINTBURN_ITEM_CODE: Joi.string().allow('').max(8),
        POINTBURN_ITEM_NAME: Joi.string().allow('').max(45),
        POINTBURN_PROMO_NUM: Joi.string().allow('').max(4),
        POINTBURN_EDC_SHOP_NAME: Joi.string().allow('').max(50),
        POINTBURN_VENDER: Joi.string().allow('').max(5),
        POINTBURN_REFERENCE_NUM: Joi.string().allow('').max(20),
        POINTBURN_APPV_NUM: Joi.string().allow('').max(6),
        POINTBURN_EDC_RATE: Joi.string().allow('').max(12),
        POINTBURN_EDC_SALE_AMOUNT: Joi.number().allow('').max(99999999999999),
        POINTBURN_EDC_DISCOUNT_AMT: Joi.number().allow('').max(99999999999999),
        POINTBURN_EDC_TERMINAL: Joi.string().allow('').max(8),
        POINTBURN_MPOINT: Joi.number().allow('').max(999999999999),
        POINTBURN_PIECE: Joi.number().allow('').max(9999),
        POINTBURN_ITEM_AMT: Joi.number().allow('').max(99999999999999),
        POINTBURN_ITEM_ADD_AMT: Joi.number().allow('').max(99999999999999),
        POINTBURN_MILE: Joi.number().allow('').max(999999999999),
        POINTBURN_AIRLINECODE: Joi.string().allow('').max(10),
        REF_WEB_NUM: Joi.string().allow('').max(21),
        SECURE_CODE: Joi.string().allow('').max(21)
    }),
    "redeem_CC": Joi.object().keys({
        MBCODE: Joi.string().allow('').max(50),
        POINTBURN_TYPE: Joi.string().allow('').max(2),
        POINTBURN_FLAG: Joi.string().allow('').max(1),
        POINTBURN_BRANCH: Joi.number().allow('').max(99),
        POINTBURN_DEPT: Joi.string().allow('').max(5),
        POINTBURN_PROMO_NAME: Joi.string().allow('').max(20),
        POINTBURN_ITEM_CODE: Joi.string().allow('').max(8),
        POINTBURN_ITEM_NAME: Joi.string().allow('').max(45),
        POINTBURN_PROMO_NUM: Joi.string().allow('').max(4),
        POINTBURN_EDC_SHOP_NAME: Joi.string().allow('').max(50),
        POINTBURN_VENDER: Joi.string().allow('').max(5),
        POINTBURN_REFERENCE_NUM: Joi.string().allow('').max(20),
        POINTBURN_APPV_NUM: Joi.string().allow('').max(6),
        POINTBURN_EDC_RATE: Joi.string().allow('').max(12),
        POINTBURN_EDC_SALE_AMOUNT: Joi.number().allow('').max(99999999999999),
        POINTBURN_EDC_DISCOUNT_AMT: Joi.number().allow('').max(99999999999999),
        POINTBURN_EDC_TERMINAL: Joi.string().allow('').max(8),
        POINTBURN_MPOINT: Joi.number().allow('').max(999999999999),
        POINTBURN_PIECE: Joi.number().allow('').max(9999),
        POINTBURN_ITEM_AMT: Joi.number().allow('').max(99999999999999),
        POINTBURN_ITEM_ADD_AMT: Joi.number().allow('').max(99999999999999),
        POINTBURN_MILE: Joi.number().allow('').max(999999999999),
        POINTBURN_AIRLINECODE: Joi.string().allow('').max(10),
        REF_WEB_NUM: Joi.string().allow('').max(21),
        SECURE_CODE: Joi.string().allow('').max(21)
    }),
    "redeem_SP": Joi.object().keys({
        MBCODE: Joi.string().allow('').max(50),
        POINTBURN_TYPE: Joi.string().allow('').max(2),
        POINTBURN_FLAG: Joi.string().allow('').max(1),
        POINTBURN_BRANCH: Joi.number().allow('').max(99),
        POINTBURN_DEPT: Joi.string().allow('').max(5),
        POINTBURN_PROMO_NAME: Joi.string().allow('').max(20),
        POINTBURN_ITEM_CODE: Joi.string().allow('').max(8),
        POINTBURN_ITEM_NAME: Joi.string().allow('').max(45),
        POINTBURN_PROMO_NUM: Joi.string().allow('').max(4),
        POINTBURN_EDC_SHOP_NAME: Joi.string().allow('').max(50),
        POINTBURN_VENDER: Joi.string().allow('').max(5),
        POINTBURN_REFERENCE_NUM: Joi.string().allow('').max(20),
        POINTBURN_APPV_NUM: Joi.string().allow('').max(6),
        POINTBURN_EDC_RATE: Joi.string().allow('').max(12),
        POINTBURN_EDC_SALE_AMOUNT: Joi.number().allow('').max(99999999999999),
        POINTBURN_EDC_DISCOUNT_AMT: Joi.number().allow('').max(99999999999999),
        POINTBURN_EDC_TERMINAL: Joi.string().allow('').max(8),
        POINTBURN_MPOINT: Joi.number().allow('').max(999999999999),
        POINTBURN_PIECE: Joi.number().allow('').max(9999),
        POINTBURN_ITEM_AMT: Joi.number().allow('').max(99999999999999),
        POINTBURN_ITEM_ADD_AMT: Joi.number().allow('').max(99999999999999),
        POINTBURN_MILE: Joi.number().allow('').max(999999999999),
        POINTBURN_AIRLINECODE: Joi.string().allow('').max(10),
        REF_WEB_NUM: Joi.string().allow('').max(21),
        SECURE_CODE: Joi.string().allow('').max(21)
    }),
    "redeem_PR": Joi.object().keys({
        MBCODE: Joi.string().allow('').max(50),
        POINTBURN_TYPE: Joi.string().allow('').max(2),
        POINTBURN_FLAG: Joi.string().allow('').max(1),
        POINTBURN_BRANCH: Joi.number().allow('').max(99),
        POINTBURN_DEPT: Joi.string().allow('').max(5),
        POINTBURN_PROMO_NAME: Joi.string().allow('').max(20),
        POINTBURN_ITEM_CODE: Joi.string().allow('').max(8),
        POINTBURN_ITEM_NAME: Joi.string().allow('').max(45),
        POINTBURN_PROMO_NUM: Joi.string().allow('').max(4),
        POINTBURN_EDC_SHOP_NAME: Joi.string().allow('').max(50),
        POINTBURN_VENDER: Joi.string().allow('').max(5),
        POINTBURN_REFERENCE_NUM: Joi.string().allow('').max(20),
        POINTBURN_APPV_NUM: Joi.string().allow('').max(6),
        POINTBURN_EDC_RATE: Joi.string().allow('').max(12),
        POINTBURN_EDC_SALE_AMOUNT: Joi.number().allow('').max(99999999999999),
        POINTBURN_EDC_DISCOUNT_AMT: Joi.number().allow('').max(99999999999999),
        POINTBURN_EDC_TERMINAL: Joi.string().allow('').max(8),
        POINTBURN_MPOINT: Joi.number().allow('').max(999999999999),
        POINTBURN_PIECE: Joi.number().allow('').max(9999),
        POINTBURN_ITEM_AMT: Joi.number().allow('').max(99999999999999),
        POINTBURN_ITEM_ADD_AMT: Joi.number().allow('').max(99999999999999),
        POINTBURN_MILE: Joi.number().allow('').max(999999999999),
        POINTBURN_AIRLINECODE: Joi.string().allow('').max(10),
        REF_WEB_NUM: Joi.string().allow('').max(21),
        SECURE_CODE: Joi.string().allow('').max(21)
    })
}
// /validation/schema/:SCHEMANO
//router.post('/:SCHEMANO', function (req, res,SCHEMANO) {
module.exports.checkSchema = (async function (req, res,SCHEMANO,dtf) {
    console.log('check schema 1');
    if (SCHEMANO == 'REGISTER') {
        res.status(200);
        res.end();
    }
    let result = Joi.validate(req.body, _mandatory_template[SCHEMANO]);
    if (result.error === null) {
        let result = Joi.validate(req.body, _template[SCHEMANO]);
        if (result.error == null) {
            //res.status(200).send('Success');
            return true;
        } else {
            console.log(result);
           /* res.json({
                "reason": "Invalid Format : " + result.error.details[0].context.key
            });*/
            //res.json(result.error.details[0].context.key);
            res.status(200);
		    res.json({
			"RESP_SYSCDE": 200,
			"RESP_DATETIME": dtf,
			"RESP_CDE": 402,
			"RESP_MSG": "Invalid Format " + result.error.details[0].context.key
		});
		return;
        }
    } else {
        console.log(result);
        //res.status(401).send('Missing Required Field');
        res.status(401);
        //res.json(result.error.details[0].context.key);
        res.json({
            "reason": "Missing Required Field : " + result.error.details[0].context.key
        });
    }
});

/*router.get('/:SCHEMANO', function(req,res){
// do something
let result = Joi.validate(req.body, _template[SCHEMANO]);
if( result.error === null ){
res.status(200);
res.end();
}
else{
console.log(result);
console.log("reason", result.value);
res.status(404);
res.json({"reason": result.value});
res.end();
}
}
});*/

//module.exports = router;