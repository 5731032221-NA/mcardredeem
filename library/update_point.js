const express = require('express');
//const router = express.Router();
const config = require('./config');
//var rp = require('request-promise');

const pool = require('node-jt400').pool(config);
//  POST /api/mcard/:MBCODE
module.exports.update_point = (async function (req, res, cal_MPOINR, cal_MBPOINT, MBCODE, dtf) {
        //router.get('/:MBCODE/:CAL_MPOINR/:CAL_MBPOINT', function(req,res){
        // get mcard
        console.log(MBCODE);
        var point_master_stmt = "update MBRFLIB/MCRS2P ";
        point_master_stmt += " set MBPOINR=?, MBPOINT=? ";
        point_master_stmt += " where MBCODE=?";
        var point_master_params = [
                cal_MPOINR,
                cal_MBPOINT,
                MBCODE
        ];


        var result = await pool.update(point_master_stmt, point_master_params);
        //console.log(result.length);
        console.log(result);
        //res.status(500).json(result);
        if (result >= 1) {
                return true;
        } else {
                console.log('Update Point Failed');
                res.status(500);
                res.json({
                        "RESP_SYSCDE": 200,
                        "RESP_DATETIME": dtf,
                        "RESP_CDE": 304,
                        "RESP_MSG": "Not success, Cannot Redeem Please Contact Membership Admin",
                        "MCARD_NUM": "",
                        "CARD_TYPE": 0,
                        "CARD_EXPIRY_DATE": "",
                        "CARD_POINT_BALANCE": "",
                        "CARD_POINT_EXPIRY": "",
                        "CARD_POINT_EXP_DATE": "",
                        "POINTBURN_MPOINT_SUCCESS": 0
                });
        }
});