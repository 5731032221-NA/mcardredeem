const express = require('express');
const router = express.Router();
const config = require('./config');
var rp = require('request-promise');
/*const config_400 = {
  host: config.db.host,
  user: config.db.user,
  password: config.db.password
      //host: '172.16.25.71',
      //user: 'qsecofr',
      //password: 'qsecofr'
};*/
const pool = require('node-jt400').pool(config);

//  POST /api/lookup/partner/nbr
//router.get('/:PARTNER_NBR/:PARTNER_ID', function(req,res){
module.exports.checkPartnerNBR = (async function (req, res, dtf) {
  // get mcard
  var stmt = "select *";
  stmt += " from MBRFLIB/PM200MP PM200MP";
  stmt += " inner join MBRFLIB/PM110MP PM110MP on PM200MP.PNID = PM110MP.PNID and PM200MP.PNNUM = PM110MP.PNNUM";
  stmt += " where PM200MP.PNNUM = '" + req.body.PARTNER_NBR + "'";

  var result = await pool.query(stmt);
  console.log(result.length);
  console.log(result);
  if (result.length > 0) {
    return true;
  } else {
    /*res.status(404);
    res.end();*/
    //res.status(200).send({
    res.status(200).json({
      "RESP_SYSCDE": 200,
      "RESP_DATETIME": dtf,
      "RESP_CDE": 301,
      "RESP_MSG": "Not success Not found Partner NBR",
      "MCARD_NUM": "",
      "CARD_TYPE": 0,
      "CARD_EXPIRY_DATE": "",
      "CARD_POINT_BALANCE": "",
      "CARD_POINT_EXPIRY": "",
      "CARD_POINT_EXP_DATE": "",
      "DEMO_TH_TITLE": "",
      "DEMO_TH_NAME": "",
      "DEMO_TH_SURNAME": "",
      "DEMO_EN_TITLE": "",
      "DEMO_EN_NAME": "",
      "DEMO_EN_SURNAME": ""
    });
  }
});