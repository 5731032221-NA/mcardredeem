const config = require('./config');
const pool = require('node-jt400').pool(config);

module.exports.checkMCard = (async function (req, res, dtf) {
    var date = dtf.substr(2, 4)
    var stmt = "select MVM01P.MBCODE,MVM01P.MBAPP,MVM01P.MBEXP,MVM01P.MBMEMC,MCRS2P.MBPOINC, MCRS2P.MBPOINR, MCRS2P.MBPOINT, MCRS2P.MBCEXP, MCRS2P.MBDATT";
    stmt += " from MBRFLIB/MVM01P MVM01P";
    stmt += " left join MBRFLIB/MCRS2P MCRS2P on MVM01P.MBCODE = MCRS2P.MBCODE";
    stmt += " where MVM01P.MBCODE ='" + req.body.MBCODE + "'";
    stmt += " and MVM01P.MBCODE not in (select MBCODE from MBRFLIB/MCRTA28P) and MVM01P.MBMEMC != 'AT'  and MVM01P.MBEXP > " + date;
    var result = await pool.query(stmt);
    if (result.length > 0) {
        console.log(result.length);
        console.log(result);
        return result;
    } else {
        res.status(200).json({
            "RESP_SYSCDE": 200,
            "RESP_DATETIME": dtf,
            "RESP_CDE": 302,
            "RESP_MSG": "Not success, not found MCard"
        });
    }

});

