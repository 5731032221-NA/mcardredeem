const express = require('express');
const router = express.Router();
const config = require('./config');
var rp = require('request-promise');

const pool = require('node-jt400').pool(config);
//  POST /api/mcard/:MBCODE
module.exports.transaction = (async function (req, res, dtf, MBAPP, ReceiptNo, MBCODE) {
        //router.post('/:DATE/:TIME/:MBAPP/:MBCODE/:MRCP/:AGEN', function(req,res){
        // get mcard
        //res.status(500).send(req);
        var date = dtf.substr(0, 8);
        var time = dtf.substr(8, 6);
        console.log(MBCODE);
        if (req.body.POINTBURN_TYPE == 'DP') {
                var point_log_params_1p = [
                        parseInt(MBAPP), //MBAPP
                        MBCODE, //MBCODE
                        parseInt(req.body.POINTBURN_BRANCH), //POINTBURN_BRANCH --> MBBRH
                        parseInt(date), //MBDAT
                        req.body.POINTBURN_ITEM_CODE, //POINTBURN_ITEM_CODE --> MBRDC
                        req.body.POINTBURN_TYPE, //POINTBUTN_TYPE --> MBTYR
                        ReceiptNo, //MBRECN
                        1, //MBRUN
                        1, // MBPOINT
                        0, //MBPIE
                        parseInt(time), //MBHOR
                        'R', //POINTBURN_FLAG --> MBFLG
                        0, //POINTBURN_MILE --> MBMILE
                        '', //MBROP
                        '', //PDNAME
                        '', //VDCODE
                        0, //MBAMT
                        parseInt(req.body.POINTBURN_MPOINT), //MBPOIND
                        parseInt(req.body.POINTBURN_EDC_DISCOUNT_AMT), //MBAMTDP
                        req.body.POINTBURN_APPV_NUM, //MBAPVO
                        req.body.POINTBURN_REFERENCE_NUM, //MBREFT
                        req.body.POINTBURN_EDC_TERMINAL, //TERMINAL3
                        req.body.POINTBURN_DEPT, //MBDEPE
                        req.body.POINTBURN_PROMO_NUM, //MBPROC
                        req.body.POINTBURN_PROMO_NAME, //MBPRON
                        parseInt(req.body.POINTBURN_EDC_SALE_AMOUNT), //MBSAMT
                        parseInt(req.body.POINTBURN_EDC_DISCOUNT_AMT), //MBUAMT
                        req.body.POINTBURN_EDC_RATE, //MBRATE,
                        //MBAGEN
                        // // // // req.body.PARTNER_NBR //PNNUM
                        // req.body.PARTNER_ID //PNID
                        req.body.REF_WEB_NUM,
                        req.body.SECURE_CODE
                ];
        } else if (req.body.POINTBURN_TYPE == 'MI') {
                var point_log_params_1p = [
                        parseInt(MBAPP), //MBAPP
                        MBCODE, //MBCODE
                        parseInt(req.body.POINTBURN_BRANCH), //POINTBURN_BRANCH --> MBBRH
                        parseInt(date), //MBDAT
                        req.body.POINTBURN_ITEM_CODE, //POINTBURN_ITEM_CODE --> MBRDC
                        req.body.POINTBURN_TYPE, //POINTBUTN_TYPE --> MBTYR
                        ReceiptNo, //MBRECN
                        1, //MBRUN
                        parseInt(req.body.POINTBURN_MPOINT), // MBPOINT
                        0, //MBPIE
                        parseInt(time), //MBHOR
                        'R', //POINTBURN_FLAG --> MBFLG
                        parseInt(req.body.POINTBURN_MILE), //POINTBURN_MILE --> MBMILE
                        req.body.POINTBURN_AIRLINECODE, //MBROP
                        '', //PDNAME
                        '', //VDCODE
                        0, //MBAMT
                        0, //MBPOIND
                        0, //MBAMTDP
                        '', //MBAPVO
                        req.body.POINTBURN_REFERENCE_NUM, //MBREFT
                        '', //TERMINAL3
                        '', //MBDEPE
                        '', //MBPROC
                        '', //MBPRON
                        0, //MBSAMT
                        0, //MBUAMT
                        '', //MBRATE,
                        //MBAGEN
                        // // // // req.body.PARTNER_NBR, //PNNUM
                        //req.body.PARTNER_ID //PNID
                        req.body.REF_WEB_NUM,
                        req.body.SECURE_CODE
                ];
        } else if (req.body.POINTBURN_TYPE == 'PR') {
                var point_log_params_1p = [
                        parseInt(MBAPP), //MBAPP
                        MBCODE, //MBCODE
                        parseInt(req.body.POINTBURN_BRANCH), //POINTBURN_BRANCH --> MBBRH
                        parseInt(date), //MBDAT
                        req.body.POINTBURN_ITEM_CODE, //POINTBURN_ITEM_CODE --> MBRDC
                        req.body.POINTBURN_TYPE, //POINTBUTN_TYPE --> MBTYR
                        ReceiptNo, //MBRECN
                        1, //MBRUN
                        parseInt(req.body.POINTBURN_MPOINT), // MBPOINT
                        parseInt(req.body.POINTBURN_PIECE), //POINTBURN_PIECE --> MBPIE
                        parseInt(time), //MBHOR
                        'R', //POINTBURN_FLAG --> MBFLG
                        0, //POINTBURN_MILE --> MBMILE
                        '', //MBROP
                        req.body.POINTBURN_ITEM_NAME, //PDNAME
                        req.body.POINTBURN_VENDER, //VDCODE
                        0, //MBAMT
                        0, //MBPOIND
                        0, //MBAMTDP
                        '', //MBAPVO
                        req.body.POINTBURN_REFERENCE_NUM, //MBREFT
                        '', //TERMINAL3
                        '', //MBDEPE
                        '', //MBPROC
                        '', //MBPRON
                        0, //MBSAMT
                        0, //MBUAMT
                        '', //MBRATE,
                        //MBAGEN
                        // // // // req.body.PARTNER_NBR, //PNNUM
                        // req.body.PARTNER_ID //PNID
                        req.body.REF_WEB_NUM,
                        req.body.SECURE_CODE
                ];
        } else if (req.body.POINTBURN_TYPE == 'SP') {
                var point_log_params_1p = [
                        parseInt(MBAPP), //MBAPP
                        MBCODE, //MBCODE
                        parseInt(req.body.POINTBURN_BRANCH), //POINTBURN_BRANCH --> MBBRH
                        parseInt(date), //MBDAT
                        req.body.POINTBURN_ITEM_CODE, //POINTBURN_ITEM_CODE --> MBRDC
                        req.body.POINTBURN_TYPE, //POINTBUTN_TYPE --> MBTYR
                        ReceiptNo, //MBRECN
                        1, //MBRUN
                        parseInt(req.body.POINTBURN_MPOINT), // MBPOINT
                        parseInt(req.body.POINTBURN_PIECE), //POINTBURN_PIECE --> MBPIE
                        parseInt(time), //MBHOR
                        'R', //POINTBURN_FLAG --> MBFLG
                        0, //POINTBURN_MILE --> MBMILE
                        '', //MBROP
                        req.body.POINTBURN_ITEM_NAME, //PDNAME
                        req.body.POINTBURN_VENDER, //VDCODE
                        parseInt(req.body.POINTBURN_ITEM_ADD_AMT), //MBAMT
                        0, //MBPOIND
                        0, //MBAMTDP
                        '', //MBAPVO
                        req.body.POINTBURN_REFERENCE_NUM, //MBREFT
                        '', //TERMINAL3
                        '', //MBDEPE
                        '', //MBPROC
                        '', //MBPRON
                        0, //MBSAMT
                        0, //MBUAMT
                        '', //MBRATE,
                        //MBAGEN
                        // // // req.body.PARTNER_NBR, //PNNUM
                        // req.body.PARTNER_ID //PNID
                        req.body.REF_WEB_NUM,
                        req.body.SECURE_CODE
                ];
        } else if (req.body.POINTBURN_TYPE == 'CC') {
                var point_log_params_1p = [
                        parseInt(MBAPP), //MBAPP
                        MBCODE, //MBCODE
                        parseInt(req.body.POINTBURN_BRANCH), //POINTBURN_BRANCH --> MBBRH
                        parseInt(date), //MBDAT
                        req.body.POINTBURN_ITEM_CODE, //POINTBURN_ITEM_CODE --> MBRDC
                        req.body.POINTBURN_TYPE, //POINTBUTN_TYPE --> MBTYR
                        ReceiptNo, //MBRECN
                        1, //MBRUN
                        parseInt(req.body.POINTBURN_MPOINT), // MBPOINT
                        parseInt(req.body.POINTBURN_PIECE), //POINTBURN_PIECE --> MBPIE
                        parseInt(time), //MBHOR
                        'R', //POINTBURN_FLAG --> MBFLG
                        0, //POINTBURN_MILE --> MBMILE
                        '', //MBROP
                        '', //PDNAME
                        '', //VDCODE
                        parseInt(req.body.POINTBURN_ITEM_AMT), //MBAMT
                        0, //MBPOIND
                        0, //MBAMTDP
                        '', //MBAPVO
                        req.body.POINTBURN_REFERENCE_NUM, //MBREFT
                        '', //TERMINAL3
                        '', //MBDEPE
                        '', //MBPROC
                        '', //MBPRON
                        0, //MBSAMT
                        0, //MBUAMT
                        '', //MBRATE,
                        //MBAGEN
                        // // // req.body.PARTNER_NBR, //PNNUM
                        // req.body.PARTNER_ID //PNID
                        req.body.REF_WEB_NUM,
                        req.body.SECURE_CODE
                ];
        }
        //res.status(500).json({"point_log_stmt_1p":"point_log_params_1p"});
        var point_log_stmt_1p = "insert into MBRFLIB/MCRR1P";
        point_log_stmt_1p += "(MBAPP,MBCODE,MBBRH,MBDAT,MBRDC,MBTYR,MBRECN,MBRUN,MBPOINT,MBPIE,MBHOR,MBFLG,MBMILE,MBROP,PDNAME,VDCODE,MBAMT,MBPOIND,MBAMTDP,MBAPVO,MBREFT,TERMINAL3,MBDEPE,MBPROC,MBPRON,MBSAMT,MBUAMT,MBRATE,MBREFW,MBSECU)";
        point_log_stmt_1p += " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        console.log(point_log_params_1p);
        // res.status(500).json({"point_log_stmt_1p":point_log_stmt_1p,"point_log_params_1p":point_log_params_1p});
        var result = await pool.insertAndGetId(point_log_stmt_1p, point_log_params_1p);
        //res.status(500).json(result);
        console.log(result.length);
        console.log(result);
        if (result == 0) {
                //res.status(500).json({"500":"intran"});
                return true;
        } else {
                console.log('Log Failed : MCRR1P');
                res.status(500);
                res.json({
                        "RESP_SYSCDE": 200,
                        "RESP_DATETIME": dtf,
                        "RESP_CDE": 500,
                        "RESP_MSG": "Log Failed : MCRR1P",
                        "MCARD_NUM": "",
                        "CARD_TYPE": 0,
                        "CARD_EXPIRY_DATE": "",
                        "CARD_POINT_BALANCE": "",
                        "CARD_POINT_EXPIRY": "",
                        "CARD_POINT_EXP_DATE": "",
                        "POINTBURN_MPOINT_SUCCESS": 0
                });
        }

});